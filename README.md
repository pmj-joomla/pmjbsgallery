# PMJ Bootstrap Gallery

PMJ Bootstrap Gallery is a little image gallery plugin for [Joomla](https://joomla.org) which uses the native [Bootstrap 4](https://getboostrap.com) carousel component and was inspired by the [SigPlus](https://extensions.joomla.org/extension/sigplus/) gallery plugin.